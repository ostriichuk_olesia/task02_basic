package com.ltd.ooo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public final class Application {

    private Application() {
    }

    public static void main(final String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(
                System.in, StandardCharsets.UTF_8));

        System.out.println("Please enter a number that will be "
                + "the start of our interval");
        int firstNumber = Integer.parseInt(br.readLine());
        System.out.println("Please enter number that will be the "
                + "last " + "in our interval (GREATER than the previous one)");
        int lastNumber = Integer.parseInt(br.readLine());

        try {
            IntervalChecker.checkIfLastNumberIsGreaterThanFirst(
                    firstNumber, lastNumber);
        } catch (WrongIntervalException e) {
            e.printStackTrace();
            System.exit(0);
        }

        System.out.println("odd numbers from the start to the end of interval");
        Calculation.printOddNumbers(Calculation.getInterval(
                firstNumber, lastNumber));
        System.out.print("\n");
        System.out.println("even numbers in reverse order");
        Calculation.printEvenNumbersInReverseOrder(
                Calculation.getInterval(firstNumber, lastNumber));
        System.out.print("\n");
        System.out.println("sum of odd numbers is " + Calculation.
                getSumOfOddNumbers(
                        Calculation.getInterval(firstNumber, lastNumber)));
        System.out.println("sum of even numbers is " + Calculation.
                getSumOfEvenNumbers(
                        Calculation.getInterval(firstNumber, lastNumber)));
        System.out.println("MAX odd number is " + Calculation.getMaxOddNumber(
                Calculation.getInterval(firstNumber, lastNumber)));
        System.out.println("MAX even number is " + Calculation.getMaxEvenNumber(
                Calculation.getInterval(firstNumber, lastNumber)));
        System.out.println("Please enter desired size of Fibonacci set");
        int sizeOfSet = Integer.parseInt(br.readLine());

        Calculation.getFibonacciSequence(Calculation.getMaxOddNumber(
                Calculation.getInterval(firstNumber, lastNumber)),
                Calculation.getMaxEvenNumber(
                        Calculation.getInterval(firstNumber, lastNumber)),
                sizeOfSet)
                .forEach(f -> System.out.print(f + " "));
        System.out.print("\n");

        Calculation.getPercentageOfOddAndEvenFibonacciNumbers(Calculation.
                getFibonacciSequence(
                        Calculation.getMaxOddNumber(
                                Calculation.getInterval(firstNumber,
                                        lastNumber)),
                        Calculation.getMaxEvenNumber(
                                Calculation.getInterval(firstNumber,
                                        lastNumber)),
                        sizeOfSet));
    }
}

