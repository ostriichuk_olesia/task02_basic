package com.ltd.ooo;

final class IntervalChecker {

    private IntervalChecker() {
    }

    static void checkIfLastNumberIsGreaterThanFirst(
            final int firstNumber, final int lastNumber)
            throws WrongIntervalException {
        if (lastNumber < firstNumber) {
            throw new WrongIntervalException(
                    "The number you entered is before start of our interval!");
        }
    }
}

