package com.ltd.ooo;

class WrongIntervalException extends Exception {

    private WrongIntervalException() {
    }

    WrongIntervalException(final String msg) {
        super(msg);
    }
}

