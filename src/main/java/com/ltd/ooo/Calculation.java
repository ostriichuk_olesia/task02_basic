package com.ltd.ooo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.IntStream;

final class Calculation {

    private static final double PERCENTAGE = 100.0;

    private Calculation() {
    }

    static int[] getInterval(final int firstNumber, final int lastNumber) {
        int[] myInterval = IntStream.rangeClosed(
                firstNumber, lastNumber).toArray();
        return myInterval;
    }

    static void printOddNumbers(final int[] myInterval) {
        for (int i = 0; i < myInterval.length; i++) {
            if (myInterval[i] % 2 != 0) {
                System.out.print(myInterval[i] + " ");
            }
        }
    }

    static void printEvenNumbersInReverseOrder(final int[] myInterval) {
        for (int i = myInterval.length - 1; i >= 0; i--) {
            if (myInterval[i] % 2 == 0) {
                System.out.print(myInterval[i] + " ");
            }
        }
    }

    static int getSumOfOddNumbers(final int[] myInterval) {
        int sumOfOddNumbers = 0;
        for (int i = 0; i < myInterval.length; i++) {
            if (myInterval[i] % 2 != 0) {
                sumOfOddNumbers += myInterval[i];
            }
        }
        return sumOfOddNumbers;
    }

    static int getSumOfEvenNumbers(final int[] myInterval) {
        int sumOfEvenNumbers = 0;
        for (int i = 0; i < myInterval.length; i++) {
            if (myInterval[i] % 2 == 0) {
                sumOfEvenNumbers += myInterval[i];
            }
        }
        return sumOfEvenNumbers;
    }

    static int getMaxOddNumber(final int[] myInterval) {
        int maxOddNumber = Integer.MIN_VALUE;
        for (int i = 0; i < myInterval.length; i++) {
            if (myInterval[i] % 2 != 0 && myInterval[i] >= maxOddNumber) {
                maxOddNumber = myInterval[i];
            }
        }
        return maxOddNumber;
    }

    static int getMaxEvenNumber(final int[] myInterval) {
        int maxEvenNumber = Integer.MIN_VALUE;
        for (int i = 0; i < myInterval.length; i++) {
            if (myInterval[i] % 2 == 0 && myInterval[i] >= maxEvenNumber) {
                maxEvenNumber = myInterval[i];
            }
        }
        return maxEvenNumber;
    }

    static Collection<Integer> getFibonacciSequence(
            final int maxOddNumber, final int maxEvenNumber,
            final int sizeOfSet) {
        Collection<Integer> fibonacciSequence =
                new ArrayList<Integer>(sizeOfSet);
        int f1 = maxOddNumber;
        fibonacciSequence.add(f1);
        int f2 = maxEvenNumber;
        fibonacciSequence.add(f2);
        int f3 = 0;
        for (int i = 1; i < sizeOfSet; i++) {
            f3 = f1 + f2;
            fibonacciSequence.add(f3);
            f1 = f2;
            f2 = f3;
        }
        return fibonacciSequence;
    }

    static void getPercentageOfOddAndEvenFibonacciNumbers(
            final Collection<Integer> fibonacciSequence) {
        int sumOfAllFibonacciNumbers = 0;
        int sumOfOddFibonacciNumbers = 0;
        int sumOfEvenFibonacciNumbers = 0;
        Iterator<Integer> myIterator = fibonacciSequence.iterator();
        while (myIterator.hasNext()) {
            int f = myIterator.next();
            sumOfAllFibonacciNumbers += f;
            if (f % 2 != 0) {
                sumOfOddFibonacciNumbers += f;
            } else {
                sumOfEvenFibonacciNumbers += f;
            }
        }
        System.out.println("Percentage of odd Fibonacci numbers is "
                + (int) Math.round(PERCENTAGE / sumOfAllFibonacciNumbers
                * sumOfOddFibonacciNumbers) + " %");
        System.out.println("Percentage of even Fibonacci numbers is "
                + (int) Math.round(PERCENTAGE / sumOfAllFibonacciNumbers
                * sumOfEvenFibonacciNumbers) + " %");
    }
}
